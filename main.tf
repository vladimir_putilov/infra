# Провайдер
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}


provider "yandex" {}


# Сеть
resource "yandex_vpc_network" "my_network" {
  name = var.network_name
}


resource "yandex_vpc_subnet" "my-subnet1-b" {
  name           = var.subnet_name
  zone           = var.zone
  network_id     = yandex_vpc_network.my_network.id
  v4_cidr_blocks = [var.v4_cidr_blocks]
}


# DNS
resource "yandex_dns_zone" "zone1" {
  name        = var.dns_zone_name
  description = "diploma dns zone"
  zone             = "putilow.ru."
  public           = true
  private_networks = [yandex_vpc_network.my_network.id]
}


resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = var.dns_rs_name1
  type    = "A"
  ttl     = 200
  data    = [var.sonarqube_ip]
}


resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = var.dns_rs_name2
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-1.network_interface.0.nat_ip_address]
}


resource "yandex_dns_recordset" "rs3" {
  zone_id = yandex_dns_zone.zone1.id
  name    = var.dns_rs_name3
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-1.network_interface.0.nat_ip_address]
}



# Nodes
resource "yandex_compute_instance" "vm-1" {
  name = var.node1_name

  resources {
    cores  = var.core_number
    memory = var.memory_number
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size = var.disk_size_number
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my-subnet1-b.id
    nat       = var.nat_state
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}


# Elasticsearch
resource "yandex_mdb_elasticsearch_cluster" "elastic" {
  name        = var.elastic_cluster_name
  environment = var.elastic_env
  network_id  = "${yandex_vpc_network.my_network.id}"

  config {

    admin_password = var.elastic_pass

    data_node {
      resources {
        resource_preset_id = var.elastic_rp_id
        disk_type_id       = var.elastic_disk_type
        disk_size          = var.elastic_disk_size
      }
    }

  }

  host {
    name = var.elastic_host_name
    zone = var.zone
    type = var.elastic_host_type
    assign_public_ip = true
    subnet_id = "${yandex_vpc_subnet.my-subnet1-b.id}"
  }

}
