variable "network_name" {}
variable "zone" {}
variable "subnet_name" {}
variable "v4_cidr_blocks" {}
variable "dns_zone_name" {}
variable "dns_rs_name1" {}
variable "dns_rs_name2" {}
variable "dns_rs_name3" {}
variable "sonarqube_ip" {}
variable "node1_name" {}
variable "core_number" {}
variable "memory_number" {}
variable "disk_size_number" {}
variable "nat_state" {}
variable "image_id" {}
variable "elastic_cluster_name" {}
variable "elastic_env" {}
variable "elastic_pass" {}
variable "elastic_rp_id" {}
variable "elastic_disk_type" {}
variable "elastic_disk_size" {}
variable "elastic_host_name" {}
variable "elastic_host_type" {}
