# Провайдер
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}


provider "yandex" {}


# Сеть
data "yandex_vpc_network" "my_network" {
  name = "my_network"
}


resource "yandex_vpc_subnet" "my_subnet" {
  name           = var.subnet_name
  zone           = var.zone
  network_id     = data.yandex_vpc_network.my_network.id
  v4_cidr_blocks = [var.v4_cidr_blocks]
}


# DNS
data "yandex_dns_zone" "zone1" {
  dns_zone_id = var.dns_zone_id
}


resource "yandex_dns_recordset" "rs1" {
  zone_id = data.yandex_dns_zone.zone1.id
  name    = var.dns_name
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.vm-1.network_interface.0.nat_ip_address]
}


# Static-IP
resource "yandex_vpc_address" "addr" {
  external_ipv4_address {
    zone_id = var.zone
  }
}

# Nodes
resource "yandex_compute_instance" "vm-1" {
  name = var.node1_name

  resources {
    cores  = var.core_number
    memory = var.memory_number
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk_size_number
    }
  }

  network_interface {
    subnet_id      = yandex_vpc_subnet.my_subnet.id
    nat            = var.nat_state
    nat_ip_address = yandex_vpc_address.addr.external_ipv4_address.0.address
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}


resource "yandex_compute_instance" "vm-2" {
  name = var.node2_name

  resources {
    cores  = var.core_number
    memory = var.memory_number
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk_size_number
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my_subnet.id
    nat       = var.nat_state
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}


# NodeGroup
resource "yandex_lb_target_group" "my_targetgroup" {
  name      = var.nodegroup_name
  region_id = var.region

  target {
    subnet_id = "${yandex_vpc_subnet.my_subnet.id}"
    address   = "${yandex_compute_instance.vm-1.network_interface.0.ip_address}"
  }
}


# LoadBalancer
resource "yandex_lb_network_load_balancer" "my_balancer" {
  name = var.loadbalancer_name

  listener {
    name = var.listener_name
    port = var.port
    external_address_spec {
      ip_version = var.ip_version
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.my_targetgroup.id}"

    healthcheck {
      name = var.healthcheck_name
      http_options {
        port = var.port
        path = var.healthcheck_path
      }
    }
  }
}
