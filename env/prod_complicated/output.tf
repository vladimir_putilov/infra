# Output
output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}


output "external_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

output "load_balancer_ip" {
  value = element(yandex_lb_network_load_balancer.my_balancer.listener[*].external_address_spec[*].address, 0)[0]
}


