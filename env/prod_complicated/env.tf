# Провайдер
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}


provider "yandex" {}


# Сеть
data "yandex_vpc_network" "my_network" {
  name = "my_network"
}


resource "yandex_vpc_subnet" "my_subnet" {
  name           = var.subnet_name
  zone           = var.zone
  network_id     = data.yandex_vpc_network.my_network.id
  v4_cidr_blocks = [var.v4_cidr_blocks]
}


# DNS
data "yandex_dns_zone" "zone1" {
  dns_zone_id = var.dns_zone_id
}


resource "yandex_dns_recordset" "rs1" {
  zone_id = data.yandex_dns_zone.zone1.id
  name    = var.dns_name
  type    = "A"
  ttl     = 200
  data    =[element(yandex_lb_network_load_balancer.my_balancer.listener[*].external_address_spec[*].address, 0)[0]]
}


# Static-IP
resource "yandex_vpc_address" "addr" {
  external_ipv4_address {
    zone_id = var.zone
  }
}


# Folder
data "yandex_resourcemanager_folder" "my_folder_1" {
  folder_id = var.folder_id
}


# Service account
data "yandex_iam_service_account" "my-service-acc-editor" {
  service_account_id = var.sa_id
}


# Nodes
resource "yandex_compute_instance" "vm-2" {
  name = var.node2_name

  resources {
    cores  = var.core_number
    memory = var.memory_number
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk_size_number
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.my_subnet.id
    nat       = var.nat_state
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}


# InstanceGroup
resource "yandex_compute_instance_group" "group1" {
  name                = var.ig1_name
  folder_id           = "${data.yandex_resourcemanager_folder.my_folder_1.id}"
  service_account_id  = "${data.yandex_iam_service_account.my-service-acc-editor.id}"
  deletion_protection = false
  instance_template {
    resources {
      memory = var.core_number
      cores  = var.memory_number
    }

    boot_disk {
      initialize_params {
        image_id = var.image_id
        size     = var.disk_size_number
      }
    }

    network_interface {
      network_id = data.yandex_vpc_network.my_network.id
      subnet_ids = [yandex_vpc_subnet.my_subnet.id]
      nat        = var.nat_state
    }

    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
  }

  scale_policy {
    auto_scale {
      initial_size = 1
      measurement_duration = 60
      cpu_utilization_target = 80
      min_zone_size = 1
      max_size = 3
      warmup_duration = 180
    }
  }

  allocation_policy {
    zones = [var.zone]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }

  load_balancer {
    target_group_name = var.nodegroup_name
  }
}


# LoadBalancer
resource "yandex_lb_network_load_balancer" "my_balancer" {
  name = var.loadbalancer_name

  listener {
    name = var.listener_name
    port = var.port
    external_address_spec {
      ip_version = var.ip_version
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.group1.load_balancer.0.target_group_id}"

    healthcheck {
      name = var.healthcheck_name
      http_options {
        port = var.port
        path = var.healthcheck_path
      }
    }
  }
}

