# infra

## Описание инфраструктуры

Для тестирования проектов подготовлен стенд в Yandex Cloud. Конфигурация проекта включает:
* Серверы с развёрнутым на нём Docker для запуска сервисов.
* Раннеры для запуска сборок GitLab CI 
* Сети и подсети
* DNS с соответствующими записями
* Балансировщика нагрузки на тестовые серверы
* Кластер Elasticsearch


Для того, чтобы создать инфраструктуру:

1. Устанавливаем terraform, ansible, git.

2. Клонируем наш репозиторий.

3. Переходим в директорию infra, создаём файл main.tfvars и заполняем его переменными из variables.tf(Как делать см. в документации Terraform)

4. Устанавливаем основную инфраструктуру:
   terraform plan -var-file="main.tfvars"
   terraform apply -var-file="main.tfvars"

5. После этого переходим в директорию env и создаём prod.tfvars и test.tfvars(сколько нужно окружений, столько и создаём), заполняем аналогично пункту 3.

6. Создаём окружения в терраформ: 
   terraform workspace new prod
   terraform workspace new test

7. Перед установкой выбираем нужное окружение и устанавливаем инфраструктуру окружения:
   terraform workspace select prod
   terraform plan -var-file="prod.tfvars"
   terraform apply -var-file="prod.tfvars"

8. Переходим в директорию ansible и создаём файл hosts(Как делать см. в документации Ansible)
   Создаём ansible.cfg с нужными настройками.
   
   [defaults]

   host_key_checking = False
   inventory         = ./hosts
   roles_path        = ./roles

9. Запускаем плейбук для установки нужного нам ПО: ansible-playbook playbook.yaml
